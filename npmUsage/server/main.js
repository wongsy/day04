//Exercise 4.4 : This is a Web server written using Express.
// Using npm to Create a package.json File and to Install the Express Module

// Defines a port to listen to
const PORT = 3000;

//load the Express module
var express = require("Express");  //add this line

//Create an Express instance called app
var app = express();  //add this line

// A function that handles requests and sends a response
// Since no path was specified, this function would handle all client requests
app.use(function (req, res) {
    res.send('It Works!! Path Hit: ' + req.originalUrl);
});

// Starts the server on localhost (default)
app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT);
});