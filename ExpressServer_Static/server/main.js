/**
 * Created by s25854 on 14/10/2016.
 */
//Exercise 4.9 Exercise: Serving a Static Web Page

//9. load the express module and create an instance of express called app.
var express = require('express');
var app = express();

//10. Bind and listen to port 3000 so that your app could respond to requests sent through localhost:3000.
const PORT = process.argv[2] || 3000;
//console.log(argv);    > node main.js 2000

//12. write the middleware that would handle serving of static files from the public folder
// Serves static files from public directory.
// __dirname is the absolute path of the application directory
app.use(express.static(__dirname + "/../client"));


/*13. create a handler for the route /about:
 a. Responds by sending this to the console: "About ACME"
 b. Passes control to the next route handler.
 c. The next route handler would terminate request/response cycle by sending this text "About ACME" back to the client.
*/
// Middleware - Router for GET /about
app.get('/about', function(req, res, next){
    console.log('About ACME');
    next();
});

// Middleware - Accommodates request on route /about by any method
app.use('/about', function(req, res){
    res.send('About ACME');
});


//14. Handle requests for undefined routes with a 404 page.
// Middleware - Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other functions)
app.use(function(req, res, next) {
    res.status(404).redirect('/404.html');
});

// Starts the server on localhost (default)
app.listen(PORT, function(){
    console.log("Server listening on: http://localhost:%s", PORT);
});

