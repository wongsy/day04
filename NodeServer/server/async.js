//Exercise 4.2
//Program to demonstrate Sync vs Async Reads
//Using Node as a server

//------- add codes start -------------
// Load/import the HTTP module
var http = require('http');  //add in this line, otherwise error: ReferenceError: http is not defined

// Port that we'll listen to
const PORT = process.argv[2] || 3000;  //add this line, otherwise error: ReferenceError: PORT is not defined
//------- add codes end -------------

// Port that we'll listen to
var fs = require('fs');

// Create server
var server = http.createServer(handleRequest);

// Function which handles requests and sends responses
function handleRequest(request, response){

    // Just printing some spaces
    console.log("\n\n");

    // so this is how Node.js's http module handles requests for specific paths
    // request.url just reads the client's target url, which happens to be stored in the request object
    // we'll learn more about request objects later
    switch(request.url){
        case '/syncread' :
            console.log("============= You've requested for /syncread ==========");
            logFilesSync();
            console.log(">>> Just left logFilesSync()");
            break;
        case '/asyncread' :
            console.log("============= You've requested for /asyncread ==========");
            logFiles();
            console.log(">>> Just left logFiles()");
            break;
        default: // do nothin but console out
            console.log("============= You've requested for %s ==========", request.url);
            break;
    }

    console.log("============= Sending response to client. Bye for now. ==========");
    response.end('It Works!! Path Hit: ' + request.url);
} // END handleRequest()

// Function to log files synchronously
function logFilesSync() {
    var  filenames,
         i;

    // fs supports async and sync versions of its methods.
    // readdirSync is the sync version of a method that reads a directory.
    // Go to https://nodejs.org/api/fs.html for more info about this module and its methods and properties.
    filenames = fs.readdirSync(".");
    for (i = 0; i < filenames.length; i++) {
        console.log("File %s is %s: ", i, filenames[i]);
    }
    console.log("Process ID is %s", process.pid);
} // END logFilesSync()

// Function to log files asynchronously
function logFiles() {
    var  filenames,
         i;

    // fs supports async and sync versions of its methods.
    // readdir is the async version of a method that reads a directory.
    // Go to https://nodejs.org/api/fs.html for more info about this module and its methods and properties.

    //----- write your codes here ------
    filenames = fs.readdir(".", function(err, files) {
        if (files) {
            for (i = 0; i < files.length; i++) {
                console.log("File %s is %s: ", i, files[i]);
            }
        } else {
            console.log("error: " + err);
        }
    });
    //---- end write codes ------

    console.log("Process ID is %s", process.pid);
} // END logFiles()

// Start server
server.listen(PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});