//Exercise 4.2
//Part 1 - Node as a web server

// Load/import the HTTP module
var http = require('http');  //add in this line, otherwise error: ReferenceError: http is not defined

// Port that we'll listen to
const PORT = process.argv[2] || 8080;

// Create server
var server = http.createServer(handleRequest);

// Function which handles requests and sends responses
function handleRequest(request, response){
    response.end('It Works!! Path Hit: ' + request.url);
}

// Start server
server.listen(PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});