//load express
var express = require('express');

//create an instance of the express application
var app = express();

//Define PORT
const PORT = 3000;

//Start the Web server on PORT 3000
app.use(function(req, res, next){
    console.log('\nThis middleware gets called ALWAYS');
    next();
});

//serve files from the public directory
//if it is not found, the proceed to the next
app.use(express.static(__dirname + '/public'));

app.get('/a', function(req, res){
    console.log('/a: route terminated');
    res.send('You requested route /a');
});

app.get('/a', function(req, res){
    console.log('/a: This will never get called');
});

app.get('/a2', function(req, res){
    console.log('/a2: Text sent is HTML formatted');
    res.send('<h1>/a2: HTML format</h1>');
});

app.get('/b', function(req, res, next){
    console.log('/b: Route not terminated');
    next();
});

app.use(function(req, res, next){
    console.log('This middleware gets called SOMETIMES');
    next();
});

app.get('/b', function(req, res, next){
    console.log('/b (Part 2): Error thrown' );
    throw new Error('/b: Route failed');
});

app.use('/b', function(err, req, res, next){
    console.log('/b: Error detected. Handling error by passing it on');
    next(err);
});

app.get('/c', function(err, req){  //req, res
    console.log('/c: Error thrown');
    throw new Error('C Failed');
});

app.use('/c', function(err, req, res, next){
    console.log('/c: Error detected but not passed on');
    next();
});

app.use(function(err, req, res, next){   //error handler has 4 arguments
    console.error("Unhandled Error Detected: " + err);
    res.status(500).json({status:500, message: 'internal error', type:'internal'});
});

app.use(function(req, res){
    console.log('Route not handled');
    res.status(404).sendFile(__dirname + '/public/404.png');
});

app.listen(PORT, function(){
    console.log('Web server started on port %s. Ctrl-C to terminate.', PORT);
    console.log('See how routing works by trying these URLs and checking your console and browser:');
    console.log('(1) localhost:3000');
    console.log('(2) localhost:3000/a');
    console.log('(3) localhost:3000/a2');
    console.log('(4) localhost:3000/b');
    console.log('(5) localhost:3000/c');
    console.log('(6) localhost:3000/foo');
});